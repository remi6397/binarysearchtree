//
//  BinarySearchTree.hpp
//  BinarySearchTree
//
//  Created by Jeremiasz Nelz on 04/01/2019.
//  Copyright © 2019 Jeremiasz Nelz. All rights reserved.
//

#pragma once

#include <memory>
#include <stdexcept>

/**
 A binary search tree
 @tparam K Type of key elements
 @tparam V Type of value elements
 */
template <class K, class V>
class BST {
    struct Node {
        K key;
        V value;
        std::unique_ptr<Node> left;
        std::unique_ptr<Node> right;

        std::unique_ptr<Node> parent;
        
        Node(K key) : key{key} {}
        
        Node(Node&& other) {
            key = std::move(other.key);
            value = std::move(other.value);
            left = std::move(other.left);
            right = std::move(other.right);
        }
        
        V& operator[](const K& k) {
            if (k == key) {
                return value;
            } else if (k < key) {
                if (!left) {
                    left = std::make_unique<Node>(Node(k));
                    left->parent = std::unique_ptr<Node>(this);
                    return left->value;
                }
                
                return (*left)[k];
            } else if (k > key) {
                if (!right) {
                    right = std::make_unique<Node>(Node(k));
                    right->parent = std::unique_ptr<Node>(this);
                    return right->value;
                }
                
                return (*right)[k];
            }

			assert(false); // never reaches this point
        }
        
        const V& operator[](const K& k) const {
            if (k == key) {
                return value;
            } else if (k < key) {
                if (!left) {
                    throw std::out_of_range("Key not found in this BST");
                }
                
                return const_cast<const Node&>(*left)[k];
            } else if (k > key) {
                if (!right) {
                    throw std::out_of_range("Key not found in this BST");
                }
                
                return const_cast<const Node&>(*right)[k];
            }

            assert(false); // never reaches this point
        }
    };
    
    std::unique_ptr<Node> root;
    
public:
    BST() {}
    
    BST(BST&& other) {
        root = std::move(other.root);
    }
    
    /**
     Find a key in the tree. If it doesn't exist yet, create it.
     @param k Key to find in the tree
     @return Reference to the value
     */
    V& operator[](const K& key) {
        if (!root) {
            root = std::make_unique<Node>(Node(key));
            return root->value;
        }
        
        return (*root)[key];
    }
    
    /**
     Find a key in the tree. If it doesn't exist yet, create it.
     @param k Key to find in the tree
     @throw @p out_of_range if the key was not found
     @return Constant reference to the value
     */
    const V& operator[](const K& key) const {
        if (!root) {
            throw std::out_of_range("Key not found in this BST");
        }
        
        return const_cast<const Node&>(*root)[key];
    }
};
