//
//  main.cpp
//  BinarySearchTree
//
//  Created by Jeremiasz Nelz on 04/01/2019.
//  Copyright © 2019 Jeremiasz Nelz. All rights reserved.
//

#include "BinarySearchTree.hpp"

#ifdef WIN32
#include <string>
#endif

#include <iostream>
#include <stdexcept>

using namespace std;

using Tree = BST<int, string>;

void printTreeValue(const Tree& tree, int key) {
    cout << "Value for " << key << " is " << tree[key] << endl;
}

int main(int argc, const char * argv[]) {
    Tree tree;
    
    int k;
    string v;

    cout << "Enter keys and values. (supply the key '0' to finish)\n";
    while (cin >> k >> v) {
        tree[k] = v;

        if (!k)
            break;
    }


    cout << "Enter keys to checkout.\n";
    while (cin >> k) {
        try {
            printTreeValue(tree, k);
        } catch (const out_of_range& oor) {
            cerr << "Key not found: " << oor.what() << endl;
        }
    }

    return 0;
}
